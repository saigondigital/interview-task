Job Description for Junior Web Developer at Saigon Digital 
==================================================================
This is a generic contact form component which needs to be coded using HTML/CSS and some Javascript if required.

We are looking for clean, valid and semantic code, you may include as many comments in the codebase as you like to explain your understanding.

Prerequisites 
===================
You will need GIT setup on your local machine to pull down the repository
The codebase only contains a simple file structure with no third party dependencies or libraries, this is done intentionally to show understanding of the fundamentals of web design and development.
You should spend approximately 2 hours on this task, if you go over or spend less time, that’s OK too! Just be fair to yourself.

Contact Form Development:
=============================
Firstly use GIT / Bitbucket to clone and pull down:
https://bitbucket.org/saigondigital/interview-task/src/master/
Create a new branch (your name, for example)

Complete the following requirements in the priority order below:
============================================================================
The form will contain four field elements - Name (Text), Email (Email), Subject (Select), Message (Textarea).
The component must be responsive for mobile, table and desktop.
Make sure the form is accessible and user friendly.
The form fields require hover, active and focus states, be as creative with this part as you see fit.
The email field must validate an email address successfully upon form submission.
The close button in the top right corner will simply “dismiss” the dialogue (Again, be creative if you wish with this part!)
If you finish all of the above, feel free to add some good old “Pop”!




